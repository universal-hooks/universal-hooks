# Universal hooks

*Руководство на других языках: [English](README.md)*

[Universal hooks](https://www.npmjs.com/package/universal-hooks) - это небольшая библиотека React-хуков, которые часто нужны в проекте. Хуки написаны с
использованием `TypeScript`. На данный момент в библиотеке представлены хуки:

- [x] [`use-toggle`](#use-toggle)
- [x] [`use-outside-event`](#use-outside-event)
- [x] [`use-keyboard-event`](#use-keyboard-event)
- [ ] use-screen-type
- [ ] use-media-query
- [ ] другие...

---

### Применение

---

## Установка

```npm
npm i universal-hooks
```

---

### use-toggle

Простой хук для управления булевым значением, предоставляет методы для установки конкретных
значений и переключения на обратное и само значение. Опционально принимает изначальное значение.

```ts
import { useToggle } from 'universal-hooks';

const { value, toggle, handleOn, handleOff, setValue } = useToggle(false)
```

---

### use-outside-event

Хук для подписки на событие за пределами элемента, принимает `ref` на элемент, `callback`
и опционально тип события (`click`, `dblclick` и остальные из `GlobalEventHandlersEventMap`)

```tsx
import { useRef } from 'react';
import { useOutsideEvent } from 'universal-hooks';

const callback = () => console.log('It works!')

const Component = () => {
  const ref = useRef(null)
  useOutsideEvent({ ref, callback, eventType: 'click' })

  return <div>
    <div ref={ref}>First div</div>
    <div>Second div</div>
  </div>
}
```

---

### use-keyboard-event

Хук, который позволяет подписываться на события клавиатуры. Принимает `key` - строка с символом/кодом
клавиши, `callback`,
опционально принимает тип события (например `keyup`, `keydown` и `keypressed`) и опционально `isCtrlOrCmdPressed` -
логическое значение, которое позволяет создавать комбинации с `ctrl` или `cmd`.

```tsx
import { useRef } from 'react';
import { useKeyboardEvent } from 'universal-hooks';

const callback = () => console.log('It works!')

const Component = () => {
  const ref = useRef(null)

  // комбинация ctrl+j
  useKeyboardEvent({
    callback,
    key: 'j',
    eventType: 'keyup',
    isCtrlOrCmdPressed: true,
  })

  // подписка на нажатие кнопки Escape
  useKeyboardEvent({
    callback,
    key: 'Escape',
    eventType: 'keypress',
  })

  return <div>
    <div ref={ref}>First div</div>
    <div>Second div</div>
  </div>
}
```