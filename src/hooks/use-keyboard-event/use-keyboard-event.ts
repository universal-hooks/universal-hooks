import { useEffect, useRef } from 'react';

type Params = {
  key: string;
  callback: (event: Event) => void;
  eventType?: 'keypress' | 'keyup' | 'keydown';
  isCtrlOrCmdPressed?: boolean;
};

export const useKeyboardEvent = ({
  key,
  callback,
  eventType = 'keydown',
  isCtrlOrCmdPressed = false,
}: Params) => {
  const callbackRef = useRef(callback);

  useEffect(() => {
    callbackRef.current = callback;
  }, [callback]);

  useEffect(() => {
    const handler: EventListener = event => {
      const isCtrlPressed =
        (event as KeyboardEvent).ctrlKey || (event as KeyboardEvent).metaKey;
      const isCtrlOrCmdRuleDone =
        !isCtrlOrCmdPressed || (isCtrlOrCmdPressed && isCtrlPressed);
      if ((event as KeyboardEvent).key === key && isCtrlOrCmdRuleDone) {
        callbackRef.current(event);
      }
    };

    document.addEventListener(eventType, handler);
    return () => document.removeEventListener(eventType, handler);
  }, [eventType, isCtrlOrCmdPressed, key]);
};
