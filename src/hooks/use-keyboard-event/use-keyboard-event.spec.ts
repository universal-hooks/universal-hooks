import { fireEvent } from '@testing-library/react';
import { renderHook } from '@testing-library/react-hooks';
import { useKeyboardEvent } from './use-keyboard-event';

let callback: (e: Event) => void;

const getEvent = (
  eventType: string,
  key: string,
  withCtrl: boolean = false
) => {
  return new KeyboardEvent(eventType, { key, ctrlKey: withCtrl });
};

beforeEach(() => {
  callback = jest.fn();
});

describe('use-keyboard-event', () => {
  test('should handle keyup event', () => {
    const event = getEvent('keyup', 'Escape');
    renderHook(() =>
      useKeyboardEvent({ key: 'Escape', callback, eventType: 'keyup' })
    );

    expect(callback).toHaveBeenCalledTimes(0);
    fireEvent(document, event);
    expect(callback).toHaveBeenCalledTimes(1);
  });

  test('should handle keyup event with ctrl(cmd)', () => {
    const event = getEvent('keyup', 'Escape', true);
    renderHook(() =>
      useKeyboardEvent({
        key: 'Escape',
        callback,
        eventType: 'keyup',
        isCtrlOrCmdPressed: true,
      })
    );

    expect(callback).toHaveBeenCalledTimes(0);
    fireEvent(document, event);
    expect(callback).toHaveBeenCalledTimes(1);
  });

  test('should handle keyup event ctrl+j', () => {
    const event = getEvent('keyup', 'j', true);
    renderHook(() =>
      useKeyboardEvent({
        key: 'j',
        callback,
        eventType: 'keyup',
        isCtrlOrCmdPressed: true,
      })
    );

    expect(callback).toHaveBeenCalledTimes(0);
    fireEvent(document, event);
    expect(callback).toHaveBeenCalledTimes(1);
  });

  test('should not handle keydown event after unmount', () => {
    const event = getEvent('keydown', 'Escape');
    const view = renderHook(() =>
      useKeyboardEvent({ key: 'Escape', callback })
    );

    expect(callback).toHaveBeenCalledTimes(0);
    fireEvent(document, event);
    expect(callback).toHaveBeenCalledTimes(1);

    jest.spyOn(document, 'removeEventListener');

    view.unmount();
    expect(document.removeEventListener).toHaveBeenCalledTimes(1);

    fireEvent(document, event);
    expect(callback).toHaveBeenCalledTimes(1);
  });

  test('shouldn`t handle unnecessary keydown event', () => {
    const event = new KeyboardEvent('keydown', {
      key: 'Enter',
    });

    renderHook(() => useKeyboardEvent({ key: 'Escape', callback }));

    expect(callback).toHaveBeenCalledTimes(0);
    fireEvent(document, event);
    expect(callback).toHaveBeenCalledTimes(0);
  });
});
