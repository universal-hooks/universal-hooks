import { act, renderHook } from '@testing-library/react-hooks';
import { useToggle } from './use-toggle';

describe('use-toggle hook', () => {
  it('should init with false value', () => {
    const { result } = renderHook(() => useToggle());
    expect(result.current.value).toBe(false);
  });

  it('should init with true value', () => {
    const { result } = renderHook(() => useToggle(true));
    expect(result.current.value).toBe(true);
  });

  it('should toggle', () => {
    const { result } = renderHook(() => useToggle());
    expect(result.current.value).toBe(false);
    act(() => {
      result.current.toggle();
    });
    expect(result.current.value).toBe(true);
  });

  it('should set true', () => {
    const { result } = renderHook(() => useToggle());
    expect(result.current.value).toBe(false);
    act(() => {
      result.current.handleOn();
    });
    expect(result.current.value).toBe(true);
  });

  it('should set false', () => {
    const { result } = renderHook(() => useToggle(true));
    expect(result.current.value).toBe(true);
    act(() => {
      result.current.handleOff();
    });
    expect(result.current.value).toBe(false);
  });

  it('should set false by toggle', () => {
    const { result } = renderHook(() => useToggle(true));
    expect(result.current.value).toBe(true);
    act(() => {
      result.current.toggle();
    });
    expect(result.current.value).toBe(false);
  });

  it('should set false directly', () => {
    const { result } = renderHook(() => useToggle(true));
    expect(result.current.value).toBe(true);
    act(() => {
      result.current.setValue(false);
    });
    expect(result.current.value).toBe(false);
  });
});
