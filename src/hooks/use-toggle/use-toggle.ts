import { useState } from 'react';

export const useToggle = (initialValue = false) => {
  const [value, setValue] = useState<boolean>(initialValue);

  const handleOff = () => setValue(false);
  const handleOn = () => setValue(true);
  const toggle = () => setValue(!value);

  return {
    value,
    setValue,
    handleOff,
    handleOn,
    toggle,
  };
};
