import { fireEvent } from '@testing-library/react';
import { renderHook } from '@testing-library/react-hooks';
import { useOutsideEvent } from './use-outside-event';

let target: HTMLDivElement;
let outside: HTMLDivElement;
let ref: { current: HTMLDivElement };
let callback = jest.fn();

beforeEach(() => {
  target = document.createElement('div');
  document.body.appendChild(target);

  outside = document.createElement('div');
  document.body.appendChild(outside);

  ref = {
    current: target,
  };
  callback = jest.fn();
});

describe('use-outside-event', () => {
  test('should handle outside click', () => {
    renderHook(() => useOutsideEvent({ ref, callback }));

    expect(callback).toHaveBeenCalledTimes(0);
    fireEvent.click(outside);
    expect(callback).toHaveBeenCalledTimes(1);
  });

  test('should handle outside dblclick', () => {
    renderHook(() => useOutsideEvent({ ref, callback, eventType: 'dblclick' }));

    expect(callback).toHaveBeenCalledTimes(0);
    fireEvent.dblClick(outside);
    expect(callback).toHaveBeenCalledTimes(1);
  });

  test('should not handle click after remove listener', () => {
    const view = renderHook(() => useOutsideEvent({ ref, callback }));

    expect(callback).toHaveBeenCalledTimes(0);
    fireEvent.click(outside);
    expect(callback).toHaveBeenCalledTimes(1);

    jest.spyOn(document, 'removeEventListener');

    view.unmount();
    expect(document.removeEventListener).toHaveBeenCalledTimes(1);

    fireEvent.click(outside);
    expect(callback).toHaveBeenCalledTimes(1);
  });

  test('should do nothing after click on the target element', () => {
    renderHook(() => useOutsideEvent({ ref, callback }));

    expect(callback).toHaveBeenCalledTimes(0);
    fireEvent.click(target);
    expect(callback).toHaveBeenCalledTimes(0);
  });
});
