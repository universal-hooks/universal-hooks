import { RefObject, useEffect, useRef } from 'react';

type Params = {
  ref: RefObject<HTMLElement | null>;
  callback: (event: Event) => void;
  eventType?: keyof DocumentEventMap;
};

const DEFAULT_EVENT = 'click';

export const useOutsideEvent = ({
  ref,
  callback,
  eventType = DEFAULT_EVENT,
}: Params) => {
  const callbackRef = useRef(callback);

  useEffect(() => {
    callbackRef.current = callback;
  }, [callback]);

  useEffect(() => {
    const handler: EventListener = event => {
      const { current: target } = ref;

      if (target && !target.contains(event.target as HTMLElement)) {
        callbackRef.current(event);
      }
    };

    document.addEventListener(eventType, handler);
    return () => document.removeEventListener(eventType, handler);
  }, [ref, eventType]);
};
